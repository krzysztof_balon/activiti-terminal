import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        String bpmnModelLocation;
        String startProcessName;
        if (args.length == 2) {
            bpmnModelLocation = args[0];
            startProcessName = args[1];
        } else {
            bpmnModelLocation = IOUtil.getString("Enter bpmn model location: ");
            startProcessName = IOUtil.getString("Enter start process name: ");
        }

        new ActivitiExecutor(new FileInputStream(bpmnModelLocation), startProcessName).execute();
    }
}
