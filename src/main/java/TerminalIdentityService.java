import org.activiti.engine.IdentityService;

public class TerminalIdentityService {
    private final IdentityService identityService;

    public TerminalIdentityService(IdentityService identityService) {
        this.identityService = identityService;
    }

    public String enterUsernameAndAuthenticate() {
        String username = IOUtil.getString("Enter your username: ");

        identityService.setAuthenticatedUserId(username);
        return username;
    }
}
