import java.util.Scanner;

public class IOUtil {
    // Only for single process app
    private static final Scanner SCANNER = new Scanner(System.in);

    public static String getString(String message) {
        System.out.print(message);
        return SCANNER.nextLine();
    }

    public static boolean getBoolean(String message) {
        String value = getString(message);
        return Boolean.parseBoolean(value);
    }
}
