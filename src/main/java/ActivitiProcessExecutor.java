import org.activiti.engine.FormService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import java.util.List;
import java.util.Map;

public class ActivitiProcessExecutor {
    private final ProcessDefinition processDefinition;
    private final FormService formService;
    private final TaskService taskService;
    private final TerminalIdentityService terminalIdentityService;

    public ActivitiProcessExecutor(ProcessDefinition processDefinition, FormService formService, TaskService taskService, TerminalIdentityService terminalIdentityService) {
        this.processDefinition = processDefinition;
        this.formService = formService;
        this.taskService = taskService;
        this.terminalIdentityService = terminalIdentityService;
    }

    public void execute(String initiator) {
        List<FormProperty> startFormProperties = formService
                .getStartFormData(processDefinition.getId()).getFormProperties();

        Map<String, String> requestedFormProperties = ActivitiUtil.fillFormProperties(startFormProperties);
        ProcessInstance processInstance = formService
                .submitStartFormData(processDefinition.getId(), requestedFormProperties);

        executeTasks(processInstance, initiator);
    }

    private void executeTasks(ProcessInstance processInstance, String username) {
        List<Task> tasks = getAvailableTasks(processInstance, username);

        while (!tasks.isEmpty()) {
            Task currentTask = selectTask(tasks);

            System.out.println("---------------------------------------------");
            System.out.println("Task " + currentTask.getName() + " started...");
            new ActivitiTaskExecutor(formService).execute(currentTask);

            tasks = getAvailableTasks(processInstance, username);
        }

        continueOrStopProcess(processInstance);
    }

    private List<Task> getAvailableTasks(ProcessInstance processInstance, String username) {
        return taskService.createTaskQuery()
                    .processInstanceId(processInstance.getId())
                    .taskAssignee(username)
                    .list();
    }

    private Task selectTask(List<Task> availableTasks) {
        return availableTasks.get(0);
    }

    private void continueOrStopProcess(ProcessInstance processInstance) {
        System.out.println("---------------------------------------------");
        boolean processContinue = IOUtil.getBoolean("Process ended - continue? (true/false): ");

        if (processContinue) {
            String username = terminalIdentityService.enterUsernameAndAuthenticate();
            executeTasks(processInstance, username);
        }
    }
}
