import org.activiti.engine.FormService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.task.Task;

import java.util.List;
import java.util.Map;

public class ActivitiTaskExecutor {
    private final FormService formService;

    public ActivitiTaskExecutor(FormService formService) {
        this.formService = formService;
    }

    public void execute(Task task) {
        List<FormProperty> taskFormProperties = formService.getTaskFormData(task.getId()).getFormProperties();
        Map<String, String> requestFormProperties = ActivitiUtil.fillFormProperties(taskFormProperties);

        formService.submitTaskFormData(task.getId(), requestFormProperties);
    }
}
