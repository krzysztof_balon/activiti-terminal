import org.activiti.engine.form.FormProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivitiUtil {

    public static Map<String, String> fillFormProperties(List<FormProperty> formProperties) {
        Map<String, String> requestedFormProperties = new HashMap<String, String>();

        for (FormProperty formProperty : formProperties) {
            if (formProperty.isWritable()) {
                String property = IOUtil.getString("Enter '" + formProperty.getName() + "': ");
                requestedFormProperties.put(formProperty.getId(), property);
            } else {
                System.out.println(formProperty.getName() + ": " + formProperty.getValue());
            }
        }
        return requestedFormProperties;
    }
}
