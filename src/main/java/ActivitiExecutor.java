import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;

import java.io.FileInputStream;

public class ActivitiExecutor {
    private final FileInputStream bpmnModelInputStream;
    private final String startProcessName;

    public ActivitiExecutor(FileInputStream bpmnModelInputStream, String startProcessName) {
        this.bpmnModelInputStream = bpmnModelInputStream;
        this.startProcessName = startProcessName;
    }

    public void execute() {
        ProcessEngine processEngine = ProcessEngineConfiguration
                .createStandaloneInMemProcessEngineConfiguration()
                .buildProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        TerminalIdentityService terminalIdentityService = new TerminalIdentityService(processEngine.getIdentityService());

        repositoryService.createDeployment()
                .addInputStream("model.bpmn20.xml", bpmnModelInputStream)
                .deploy();

        ProcessDefinition startProcessDef = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(startProcessName)
                .singleResult();

        String initiator = terminalIdentityService.enterUsernameAndAuthenticate();
        new ActivitiProcessExecutor(startProcessDef,
                processEngine.getFormService(),
                processEngine.getTaskService(),
                terminalIdentityService
        ).execute(initiator);
    }
}
